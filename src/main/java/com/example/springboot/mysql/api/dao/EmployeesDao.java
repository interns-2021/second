package com.example.springboot.mysql.api.dao;

import org.springframework.data.repository.CrudRepository;

import com.example.springboot.mysql.api.model.Employees;

public interface EmployeesDao extends CrudRepository<Employees, Integer> {
	
	

}
