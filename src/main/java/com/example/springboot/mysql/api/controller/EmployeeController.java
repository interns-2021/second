package com.example.springboot.mysql.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.mysql.api.dao.EmployeesDao;
import com.example.springboot.mysql.api.model.Employees;

@RestController
@EnableCaching
@RequestMapping("/Employees")
public class EmployeeController {
	@Autowired
	private EmployeesDao dao;
	
	@PostMapping("/addEmployees")
	public String saveEmployees(@RequestBody List<Employees> employee) {
		dao.saveAll(employee);
		return "Added employee :  " + employee.size();
		
	}
	
	@GetMapping("/getEmployees")
	public List<Employees> getEmployees(){
		return (List<Employees>) dao.findAll();
	}
	
	
	
}
