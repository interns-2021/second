package com.example.springboot.mongo.api.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.mongo.api.model.Employees;
import com.example.springboot.mongo.api.repository.EmployeesRepositary;

@RestController
@EnableCaching
public class EmployeesController {
	
	@Autowired
	private EmployeesRepositary repository; 
	
	@PostMapping("/addEmployees")
	public String saveEmployees(@RequestBody Employees employee) {
		
		repository.save(employee);
		return "Added employee with id "+employee.id;
		
	}
	
	@GetMapping("/FindAllEmployees")
	public List<Employees> getEmployees(){
		return repository.findAll();
	}
	
	@GetMapping("/FindAllEmployees/{id}")
	@Cacheable(key = "#id",value = "Employees")
	public Optional<Employees> getEmployees(@PathVariable int id){
		return repository.findById(id);
	}
	
	@DeleteMapping("/delete/{id}")
	@CacheEvict(key = "#id",value = "Employees")
	public String deleteEmployees(@PathVariable int id) {
		repository.deleteById(id);
		return "Employee deleted with id: " + id;
		
	}
	

}
