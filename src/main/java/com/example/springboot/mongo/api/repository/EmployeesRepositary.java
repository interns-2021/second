package com.example.springboot.mongo.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.springboot.mongo.api.model.Employees;

public interface EmployeesRepositary extends MongoRepository<Employees, Integer>{

}
